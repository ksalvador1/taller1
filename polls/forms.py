from django import forms
from polls.models import Team,Player,Coach

class TeamForm(forms.ModelForm):
    class Meta:
        model= Team
        fields = '__all__'

class PlayerForm(forms.ModelForm):
    class Meta:
        model= Player
        fields = '__all__'

class CoachForm(forms.ModelForm):
    class Meta:
        model= Coach
        fields = '__all__'