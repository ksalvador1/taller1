from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Coach(models.Model):

    
    Nombre_Coach = models.CharField(max_length=200,blank=True ,null=True)
    Edad_Coach = models.PositiveIntegerField(blank=True ,null=True) 
    Email_Coach = models.EmailField(max_length=200, blank=True ,null=True)
    Rut_Coach = models.CharField(max_length=200, blank=True ,null=True)
    Apodo_Coach = models.CharField(max_length=200, blank=True ,null=True)
    user_Coach = models.OneToOneField(User, blank=True ,null=True, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date created', default=timezone.now)
    

    def __str__(self):
        return str(self.Nombre_Coach)

class Team(models.Model):
    Nombre_Team = models.CharField(max_length=200)
    Descripcion_Team = models.TextField()
    Logo_Team = models.ImageField(upload_to= "Fotos_Team/",blank=True, null=True)
    Code_Team = models.CharField(max_length=20,unique=True)
    Coach_Team = models.OneToOneField(Coach, on_delete=models.CASCADE)

    def __str__(self):
        return self.Code_Team

POSICION_PLAYER = (
    ('BA', 'Base'),
    ('ES', 'Escolta'),
    ('AL', 'Alero'),
    ('AP', 'Ala-pivot'),
    ('PI', 'Pivot'),
)

class Player(models.Model):
    Nombre_Player = models.CharField(max_length=200)
    Apodo_Player = models.CharField(max_length=200)
    Nacimiento_Player = models.DateField()
    Edad_Player =models.PositiveIntegerField()
    Rut_Player = models.CharField(max_length=12)
    Email_Player = models.EmailField()
    Estatura_Player = models.PositiveIntegerField(help_text='Altura en cm')
    Peso_Player = models.PositiveIntegerField(help_text='En gramos')
    Foto_Player = models.ImageField(upload_to= "Fotos_Player/",blank=True, null=True)
    Posicion_Player = models.CharField(choices=POSICION_PLAYER, max_length=2)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date created', default=timezone.now)

    def __str__(self):
        return '{Nombre_Player} ({team})'. format(Nombre_Player=self.Nombre_Player, team=self.team)
    



class Roster(models.Model):
    name = models.CharField(max_length=150)
    player = models.ManyToManyField(Player)

    def __str__(self):
        return self.name

class Partido(models.Model):
    Nombre_Partido = models.CharField(max_length=200)
    team1 = models.ForeignKey(Roster,related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Roster,related_name='team2', on_delete=models.CASCADE)
    fecha = models.DateField()
    created_date = models.DateTimeField('date created', default=timezone.now)
    def __str__(self):
        return '{name} ({team1} vs {team2})'.format(
            name=self.Nombre_Partido,
            team1=self.team1,
            team2=self.team2

        )