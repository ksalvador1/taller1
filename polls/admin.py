from django.contrib import admin
from django.utils.safestring import mark_safe

from polls.models import Team
from polls.models import Player
from polls.models import Coach
from polls.models import Partido
from polls.models import Roster

@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display =('Nombre_Team','Descripcion_Team','Logo','Code_Team')
    search_fields =['Nombre_Team']

    def Logo(self,team):
        return mark_safe("<img src='/media/{}'  width='60' height='60' />".format(team.Logo_Team))
    
    

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display =('Nombre_Player','Apodo_Player','Edad_Player','Rut_Player','Email_Player','Estatura_Player','Peso_Player','Foto','Posicion_Player')
    search_fields =['Nombre_Player','Apodo_Player','Rut_Player']
    list_filter =('Edad_Player','Nacimiento_Player')

    def Foto(self,player):
        return mark_safe("<img src='/media/{}'  width='60' height='60' />".format(player.Foto_Player))
   

    


@admin.register(Roster)
class RosterAdmin(admin.ModelAdmin):
    pass    

@admin.register(Partido)
class PartidoAdmin(admin.ModelAdmin):
    pass
