# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect
from polls.models import Team, Player , Partido, Coach 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from polls.forms import TeamForm,PlayerForm, CoachForm
from django.contrib.auth.decorators import login_required , permission_required 
from django.contrib import messages
from django.contrib.auth.models import User


@login_required
def home(request):
    template_name = 'home.html'
    data = {}
    SU=request.user.is_superuser
    if SU==True:
        data ['match'] = Partido.objects.all().order_by('created_date').reverse()[:3]
        data ['players'] = Player.objects.all().order_by('created_date').reverse()[:3]  
        data ['coachs'] = Coach.objects.all().order_by('created_date').reverse()[:3]
    else :
        data ['players'] = Player.objects.all().order_by('created_date').reverse()[:3]
        data ['match'] = Partido.objects.all().order_by('created_date').reverse()[:3] 

    return render(request, template_name, data)
@login_required
def Team_list(request):
    template_name ='Team_list.html'
    data={}
    print("User login:", request.user )
    data['title'] ='List Teams'
    tm= Team.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(tm, 1)
    try:
        data['Team'] = paginator.page(page)
    except PageNotAnInteger:
        data['Team'] = paginator.page(1)
    except EmptyPage:
        data['Team'] = paginator.page(paginator.num_pages)
    return render(request, template_name, data)
@login_required
def Player_list(request):
    template_name = 'Player_list.html'
    data = {}
    data ['title']= 'List Players'
    data ['players'] = Player.objects.all().order_by('team')
    data ['teams']= Team.objects.all().order_by('Nombre_Team')
    tm= Team.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(tm, 1)
    try:
        data['teams'] = paginator.page(page)
    except PageNotAnInteger:
        data['teams'] = paginator.page(1)
    except EmptyPage:
        data['teams'] = paginator.page(paginator.num_pages)
    return render(request, template_name, data)
    

@login_required
def Coach_list(request):
    template_name = 'Coach_list.html'
    data = {}
    data ['title']= 'List Coach'
    data['coachs'] = Coach.objects.all()
    
    tm= Coach.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(tm, 1)
    try:
        data['coachs'] = paginator.page(page)
    except PageNotAnInteger:
        data['coachs'] = paginator.page(1)
    except EmptyPage:
        data['coachs'] = paginator.page(paginator.num_pages)
    return render(request, template_name, data)
    
@login_required
def Partido_list(request):
    template_name = 'Partido_list.html'
    data = {}
    data ['title']= 'List Partido'
    data ['match'] = Partido.objects.all()
    return render(request, template_name, data)
@login_required
def Last5_list(request):
    template_name = 'Partido_list.html'
    data = {}
    data ['title']= 'List Partido'
    data ['match'] = Partido.objects.all().order_by('created_date').reverse()[:5]
    return render(request, template_name, data)
@permission_required('polls.createTeam')    
def createTeam (request):
    data = {}
    template_name ='createTeam.html'
    data['form'] = TeamForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(request, messages.SUCCESS, 'Equipo guardado correctamente')
        return redirect('polls:Team_list')
        
    return render(request, template_name, data)
@permission_required('polls.teamupdate') 
def teamupdate (request,pk):
    data = {}
    template_name ='teamupdate.html'
    team = Team.objects.get(pk=pk)
    data['form'] = TeamForm(request.POST or None, instance=team)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(request, messages.SUCCESS, 'Equipo guardado correctamente')
        return redirect('polls:Team_list')
    return render(request, template_name,data)
@permission_required('polls.teamdelete')
def teamdelete(request,pk):
    data = {}
    template_name = 'teamdelete.html'
    data['team'] = Team.objects.get(pk=pk)
    if request.method =='POST':
        data['team'].delete()
        messages.add_message(request, messages.ERROR, 'Equipo ELIMINADO correctamente')
        return redirect('polls:Team_list')
    return render(request, template_name,data)
@permission_required('polls.playercreate')
def playercreate(request):
    data = {}
    template_name ='playercreate.html'
    data['form'] = PlayerForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(request, messages.SUCCESS, 'Player guardado correctamente')
        return redirect('polls:Player_list')
        
    return render(request, template_name, data)
@permission_required('polls.playerupdate')
def playerupdate (request,pk):
    data = {}
    template_name ='playerupdate.html'
    player = Player.objects.get(pk=pk)
    data['form'] = PlayerForm(request.POST or None, instance=player)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(request, messages.SUCCESS, 'Player guardado correctamente')
        return redirect('polls:Player_list')
    return render(request, template_name,data)
@permission_required('polls.playerdelete')
def playerdelete(request,pk):
    data = {}
    template_name = 'playerdelete.html'
    data['player'] = Player.objects.get(pk=pk)
    if request.method =='POST':
        data['player'].delete()
        messages.add_message(request, messages.ERROR, 'Player eliminado correctamente')
        return redirect('polls:Player_list')
    return render(request, template_name,data)

@permission_required('polls.coachcreate')
def coachcreate(request):
    data = {}
    template_name ='coachcreate.html'
    data['form'] = CoachForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        return redirect('polls:Coach_list')
        
    return render(request, template_name, data)
@permission_required('polls.change_coach',login_url='/auth/login')
def coachupdate (request,pk):
    data = {}
    template_name ='coachupdate.html'
    coachs = Coach.objects.get(pk=pk)
    data['form'] = CoachForm(request.POST or None, instance=coachs)
    if data['form'].is_valid():
        data['form'].save()
        return redirect('polls:Coach_list')
    return render(request, template_name,data)
@permission_required('polls.coachdelete')
def coachdelete(request,pk):
    data = {}
    template_name = 'coachdelete.html'
    data['coach'] = Coach.objects.get(pk=pk)
    if request.method =='POST':
        data['coach'].delete()
        return redirect('polls:Coach_list')
    return render(request, template_name,data)