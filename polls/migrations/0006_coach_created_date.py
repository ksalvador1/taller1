# Generated by Django 3.0.6 on 2020-06-13 23:06

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0005_player_created_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='coach',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created'),
        ),
    ]
