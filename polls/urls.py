
from django.urls import path 
from polls import views

app_name = 'polls'

urlpatterns = [
    path('home/', views.home, name='home'),
    path('partidos/', views.Partido_list, name='Partido_list'),
    path('teams/', views.Team_list, name='Team_list'),
    path('players/', views.Player_list, name='Player_list'),
    path('playercreate/', views.playercreate, name='playercreate'),
    path('playerupdate/<int:pk>', views.playerupdate, name='playerupdate'),
    path('coachs/', views.Coach_list, name='Coach_list'),
    path('coachdelete/<int:pk>', views.coachdelete, name='coachdelete'),
    path('coachcreate/', views.coachcreate, name='coachcreate'),
    path('coachupdate/<int:pk>', views.coachupdate, name='coachupdate'),
    path('last5/', views.Last5_list, name='Last5_list'),
    path('createTeam/', views.createTeam, name='createTeam'),
    path('teamupdate/<int:pk>', views.teamupdate, name='teamupdate'),
    path('teamdelete/<int:pk>', views.teamdelete, name='teamdelete'),
    path('playerdelete/<int:pk>', views.playerdelete, name='playerdelete'),
]
